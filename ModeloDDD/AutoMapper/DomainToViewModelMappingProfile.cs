﻿using AutoMapper;
using ModeloDDD.Domain.Entities;
using ModeloDDD.ViewModels;

namespace ModeloDDD.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ClienteViewModel, Cliente>();
            CreateMap<ProdutoViewModel, Produto>();
        }
    }
}