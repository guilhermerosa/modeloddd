﻿using AutoMapper;
using ModeloDDD.Domain.Entities;
using ModeloDDD.ViewModels;


namespace ModeloDDD.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Cliente, ClienteViewModel>();
            CreateMap<Produto, ProdutoViewModel>();
        }
    }
}