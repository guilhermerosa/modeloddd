﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ModeloDDD.ViewModels
{
    public class ProdutoViewModel
    {
        [Key]
        public int ProdutoId { get; set; }

        [Required(ErrorMessage ="Preencha o campo Nome")]
        [MaxLength(250,ErrorMessage ="Máximo {0} caracteres")]
        [MinLength(2, ErrorMessage ="Mínimo {0} caracteres")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Preencha um Valor")]
        [Range(typeof(decimal),"0","999999999999")]
        [DisplayFormat(DataFormatString = "{0:R$#.##}")]
        public decimal Valor { get; set; }

        [DisplayName("Disponível?")]
        public bool Disponivel { get; set; }

        public int ClienteId { get; set; }
        public virtual ClienteViewModel Cliente { get; set; }
    }
}